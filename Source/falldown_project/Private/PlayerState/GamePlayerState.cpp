#include "PlayerState/GamePlayerState.h"

AGamePlayerState::AGamePlayerState()
{
	CurrentHp = MaxHp = 100.0f;
}

void AGamePlayerState::SetGameScore(float value)
{
	// 사망 상태인 경우 함수 호출 종료
	if (IsDead) return;

	// 점수가 0 미만이 되지 않도록 합니다.
	if (value < 0.0f)
		value = 0.0f; 

	// 점수 설정
	SetScore(value);

	// 점수 변경 이벤트 발생
	OnScoreChanged.Broadcast(value);
}

void AGamePlayerState::SetCurrentHp(float value)
{
	// 사망 상태인 경우 함수 호출 종료
	if (IsDead) return;

	// 체력을 설정합니다.
	CurrentHp = value;

	// 체력의 값이 0 ~ 최대체력 사이의 값이 될수 있도록 합니다.
	CurrentHp = FMath::Clamp(CurrentHp, 0, MaxHp);


	// 체력 변경 이벤트 발생
	OnHpChanged.Broadcast(CurrentHp, MaxHp);

	// 체력이 0인 경우
	if (FMath::IsNearlyEqual(CurrentHp, 0.0f))
	{
		// 사망 상태로 설정합니다.
		IsDead = true;

		// 게임 오버 이벤트 발생
		OnGameOver.Broadcast(GetScore());
	}
}

void AGamePlayerState::TogglePause()
{
	// 일시정지 상태 변경
	IsPaused = !IsPaused;

	// 일시정지 이벤트 발생
	OnPaused.Broadcast(IsPaused);
}
