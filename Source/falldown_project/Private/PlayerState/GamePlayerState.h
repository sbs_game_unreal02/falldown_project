// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "../../falldown_project.h"
#include "GamePlayerState.generated.h"

DECLARE_EVENT_OneParam(AGamePlayerState, FOneParamActionFloat, float)
DECLARE_EVENT_OneParam(AGamePlayerState, FOneParamActionBool, bool)
DECLARE_EVENT_TwoParams(AGamePlayerState, FTwoParamActionFloatFloat, float ,float)

UCLASS()
class AGamePlayerState final : public APlayerState
{
	GENERATED_BODY()


private:
	// 일시정지 상태에 대한 변수입니다.
	UPROPERTY()
	bool IsPaused;

	// 최대 체력을 나타냅니다.
	UPROPERTY()
	float MaxHp;

	// 현재 체력을 나타냅니다.
	UPROPERTY()
	float CurrentHp;

	// 사망 상태를 나타냅니다.
	UPROPERTY()
	float IsDead;


public :
	// 점수 변경 이벤트
	FOneParamActionFloat OnScoreChanged;

	// 일시정지 이벤트
	FOneParamActionBool OnPaused;

	// 체력 변경 이벤트
	FTwoParamActionFloatFloat OnHpChanged;

	// 게임 오버 이벤트
	FOneParamActionFloat OnGameOver;


public :
	AGamePlayerState();

	// 점수를 설정합니다.
	void SetGameScore(float value);

	// 점수를 가져옵니다.
	FORCEINLINE float GetGameScore() const { return GetScore(); }

	// 최대 체력을 반환합니다.
	FORCEINLINE float GetMaxHp() const { return MaxHp; }

	// 현재 체력을 반환합니다.
	FORCEINLINE float GetCurrentHp() const { return CurrentHp; }

	// 체력을 설정합니다.
	void SetCurrentHp(float value);

	
	// 일시정지 토글
	void TogglePause();

	// 일시정지 상태를 확인합니다.
	FORCEINLINE bool GetPauseState() const { return IsPaused; }

};
