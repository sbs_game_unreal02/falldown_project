#include "AnimInstance/PlayerCharacterAnimInstance.h"

#include "Actor/PlayerCharacter.h"
#include "Component/PlayerCharacterMovementComponent.h"

void UPlayerCharacterAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	// 이 애님인스턴스를 사용하는 Pawn 객체를 얻습니다.
	APawn* pawn = TryGetPawnOwner();
	if (!IsValid(pawn)) 
	{
		UE_LOG(LogTemp, Error, TEXT("owner pawn is not valid!"));
		return;
	}

	// 얻은 Pawn 객체를 이용하여 PlayerCharacter 객체를 얻습니다.
	APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(pawn);

	// 최대 이동 속력 초기화
	MaxSpeed = playerCharacter->GetPlayerCharacterMovementComponent()->GetMoveSpeed(); 

	// 이동 콜백 등록
	playerCharacter->OnHorizontalInputEvent.AddUObject(
		this, &ThisClass::CALLBACK_OnHorizontalInput);
}

void UPlayerCharacterAnimInstance::NativeUpdateAnimation(float ds)
{
	Super::NativeUpdateAnimation(ds);



}

void UPlayerCharacterAnimInstance::CALLBACK_OnHorizontalInput(float axis)
{
	CurrentSpeed = MaxSpeed * FMath::Abs(axis);
}
