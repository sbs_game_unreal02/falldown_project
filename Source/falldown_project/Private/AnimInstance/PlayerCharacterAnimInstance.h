// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PlayerCharacterAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class UPlayerCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
private :
	// 최대 이동 속력을 나타냅니다.
	float MaxSpeed;

protected :
	// 이 AnimInstance  를 사용하는 Pawn 의 속력을 나타냅니다.
	// 블루프린트에서 읽기 전용으로 이 필드를 노출시킵니다.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float CurrentSpeed;

public :
	virtual void NativeBeginPlay() override;

	// 애니메이션 블루프린트 틱
	virtual void NativeUpdateAnimation(float ds) override;

private :
	UFUNCTION()
	void CALLBACK_OnHorizontalInput(float axis);
};
