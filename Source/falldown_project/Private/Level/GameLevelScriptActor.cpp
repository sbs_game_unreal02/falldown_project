#include "Level/GameLevelScriptActor.h"

void AGameLevelScriptActor::GetLeftRightPoint(FVector& out_leftPoint, FVector& out_rightPoint)
{
	out_leftPoint = MapLeftPoint->GetActorLocation();
	out_rightPoint = MapRightPoint->GetActorLocation();
}
