// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "GameLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class AGameLevelScriptActor final : public ALevelScriptActor
{
	GENERATED_BODY()

protected :
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* MapLeftPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* MapRightPoint;

public :
	// 맵의 왼쪽 / 오른쪽 위치를 반환합니다.
	// out_leftPoint : 출력용 매개 변수이며 맵 왼쪽 지점에 대한 위치를 반환합니다.
	// out_rightPoint : 출력용 매개 변수이며 맵 오른쪽 지점에 대한 위치를 반환합니다.
	void GetLeftRightPoint(FVector& out_leftPoint, FVector& out_rightPoint);


	
};
