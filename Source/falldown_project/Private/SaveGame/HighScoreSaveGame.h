// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "HighScoreSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class UHighScoreSaveGame final : public USaveGame
{
	GENERATED_BODY()
	
public :
	static const FString SlotName;
	static const int32 UserIndex;


public :
	// 최고 점수를 나타냅니다.
	UPROPERTY()
	float HighScore;

};
