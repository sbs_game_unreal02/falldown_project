#include "Widget/MainLevel/MainLevelScoreWidget.h"
#include "Components/TextBlock.h"

void UMainLevelScoreWidget::InitializeWidget(float highScore)
{
	FText scoreText;

	// 최고 점수 기록이 존재하는 경우
	if (highScore > 0)
	{
		FString scoreToString = FString::Printf(TEXT("%.2f"), highScore);
		scoreText = FText::FromString(scoreToString);
	}
	// 최고 점수 기록이 존재하지 않는 경우
	else scoreText = FText::FromString(TEXT("- -"));

	TextBlock_HighScore->SetText(scoreText);
}
