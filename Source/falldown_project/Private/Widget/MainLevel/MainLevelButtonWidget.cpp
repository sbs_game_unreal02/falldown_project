#include "Widget/MainLevel/MainLevelButtonWidget.h"
#include "Components/Button.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"

void UMainLevelButtonWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Button_GameStart->OnClicked.AddUniqueDynamic(
		this, &ThisClass::CALLBACK_OnStartButtonClicked);
	Button_Quit->OnClicked.AddUniqueDynamic(
		this, &ThisClass::CALLBACK_OnQuitButtonClicked);
}

void UMainLevelButtonWidget::CALLBACK_OnStartButtonClicked()
{
	// GameLevel 로 전환합니다.
	UGameplayStatics::OpenLevel(GetWorld(), TEXT("GameLevel"));
}

void UMainLevelButtonWidget::CALLBACK_OnQuitButtonClicked()
{
	UKismetSystemLibrary::QuitGame(
		GetWorld(),
		GetWorld()->GetFirstPlayerController(),
		EQuitPreference::Quit,
		false);
}
