#include "Widget/MainLevel/MainLevelWidget.h"
#include "Animation/WidgetAnimation.h"
#include "Widget/MainLevel/MainLevelScoreWidget.h"

void UMainLevelWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// 위젯 애니메이션을 재생합니다.
	PlayAnimation(Anim_Logo, 0.0f, 0);
}

void UMainLevelWidget::InitializeWidget(float highScore)
{
	Widget_MainLevelScore->InitializeWidget(highScore);
}
