// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainLevelScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMainLevelScoreWidget final : public UUserWidget
{
	GENERATED_BODY()
	
protected :
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* TextBlock_HighScore;

public :
	void InitializeWidget(float highScore);

};
