// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainLevelButtonWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMainLevelButtonWidget final : public UUserWidget
{
	GENERATED_BODY()

protected :
	// 게임 시작 버튼
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* Button_GameStart;
	
	// 게임 종료 버튼
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* Button_Quit;



protected:
	virtual void NativeConstruct() override;

private :
	UFUNCTION()
	void CALLBACK_OnStartButtonClicked();

	UFUNCTION()
	void CALLBACK_OnQuitButtonClicked();


};
