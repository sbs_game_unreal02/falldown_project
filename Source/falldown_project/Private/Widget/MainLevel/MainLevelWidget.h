// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainLevelWidget.generated.h"

/**
 * 
 */
UCLASS()
class UMainLevelWidget final : public UUserWidget
{
	GENERATED_BODY()

protected :
	// 로고 애니메이션을 나타냅니다.
	UPROPERTY(BlueprintReadOnly, meta = (BindWidgetAnim), Transient)
	class UWidgetAnimation* Anim_Logo;

	// 최고 점수 위젯
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget), Transient)
	class UMainLevelScoreWidget* Widget_MainLevelScore;

protected :
	virtual void NativeConstruct() override;

public :
	void InitializeWidget(float highScore);

	
};
