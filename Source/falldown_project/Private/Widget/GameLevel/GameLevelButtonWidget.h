#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameLevelButtonWidget.generated.h"

// 일시정지 버튼 클릭 이벤트 형식
DECLARE_EVENT(UGameLevelButtonWidget, FPauseButtonClickEventSignature)

UCLASS()
class UGameLevelButtonWidget final : public UUserWidget
{
	GENERATED_BODY()

public :
	// 일시정지 버튼 클릭 이벤트
	FPauseButtonClickEventSignature OnPauseButtonClicked;

private :
	UPROPERTY()
	class AGamePlayerState* GamePlayerState;

protected :
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton* Button_Home;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UButton * Button_Pause;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage* Image_Pause;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage* Image_Play;

protected :
	virtual void NativeConstruct() override;

public :
	// 버튼 위젯을 초기화합니다.
	void InitializeWidget(class AGamePlayerState* playerState);

private :
	// 일시정지 버튼 이미지를 상태에 따라 갱신합니다.
	void UpdatePauseButtonImage();

	// 홈 버튼 표시 여부를 상태에 따라 갱신합니다.
	void UpdateHomeButtonVisibility(bool bVisible);

	UFUNCTION()
	void CALLBACK_OnPauseButtonClicked();

	UFUNCTION()
	void CALLBACK_OnHomeButtonClicked();
	
};

// 문제 1
// 일시정지 시
// 홈 버튼이 표시되도록 합니다.
// 일시정지 상태가 아닌 경우
// 홈 버튼이 표시되지 않도록 합니다.

// 문제2
// 홈 버튼 클릭 시 MainLevel 로 이동합니다.
// 레벨 전환에 대한 내용은 CALLBACK_OnHomeButtonClicked() 함수에서 진행합니다.