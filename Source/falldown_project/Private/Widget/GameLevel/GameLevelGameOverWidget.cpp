#include "Widget/GameLevel/GameLevelGameOverWidget.h"
#include "PlayerState/GamePlayerState.h"

void UGameLevelGameOverWidget::InitializeWidget(AGamePlayerState* playerState)
{
	playerState->OnGameOver.AddUObject(
		this,
		&ThisClass::CALLBACK_OnGameOver);
}

void UGameLevelGameOverWidget::NativeConstruct()
{
	Super::NativeConstruct();

	SetVisibility(ESlateVisibility::Hidden);
}

void UGameLevelGameOverWidget::CALLBACK_OnGameOver(float score)
{
	SetVisibility(ESlateVisibility::Visible);

}
