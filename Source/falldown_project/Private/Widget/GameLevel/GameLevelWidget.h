#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameLevelWidget.generated.h"

/**
 * 
 */
UCLASS()
class UGameLevelWidget final : public UUserWidget
{
	GENERATED_BODY()

protected :
	// ���� ����
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UGameLevelScoreWidget* Widget_GameLevelScore;

	// ��ư ����
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UGameLevelButtonWidget* Widget_GameLevelButton;

	// Hp ����
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UGameLevelHpWidget* Widget_GameLevelHp;

	// GameOver ����
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UGameLevelGameOverWidget* Widget_GameLevelGameOver;



protected :
	virtual void NativeConstruct() override;

public :
	void InitializeWidget(class AGamePlayerState* playerState);
	
};
