#include "Widget/GameLevel/GameLevelScoreWidget.h"
#include "PlayerState/GamePlayerState.h"

#include "Components/TextBlock.h"

void UGameLevelScoreWidget::InitializeWidget(AGamePlayerState* playerState)
{
	// 점수 변경 콜백 등록
	playerState->OnScoreChanged.AddUObject(this, &ThisClass::CALLBACK_OnScoreChanged);
}

void UGameLevelScoreWidget::CALLBACK_OnScoreChanged(float newScore)
{
	// 정수 형식으로 변환합니다.
	int32 intScore = newScore;

	// 화면에 표시할 점수 텍스트
	FText scoreText = FText::FromString(
		FString::Printf(TEXT("%d"), intScore));
	// FString::Printf() : 지정한 서식으로 문자열을 완성하여 FString 형식으로 반환합니다.
	// FText::FromString() : 전달한 FString 형식 데이터를 FText 형식으로 변환합니다.

	// 텍스트를 변경합니다.
	TextBlock_Score->SetText(scoreText);
}
