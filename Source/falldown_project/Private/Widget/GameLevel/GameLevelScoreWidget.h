// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameLevelScoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class UGameLevelScoreWidget final : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UTextBlock* TextBlock_Score;

public :
	// 점수 위젯을 초기화합니다.
	void InitializeWidget(class AGamePlayerState* playerState);

private :
	// 점수 변경 시 호출되는 함수입니다.
	UFUNCTION()
	void CALLBACK_OnScoreChanged(float newScore);
};