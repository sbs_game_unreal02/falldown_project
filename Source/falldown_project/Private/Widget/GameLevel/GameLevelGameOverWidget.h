// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameLevelGameOverWidget.generated.h"

/**
 * 
 */
UCLASS()
class UGameLevelGameOverWidget final : public UUserWidget
{
	GENERATED_BODY()

public :
	void InitializeWidget(class AGamePlayerState* playerState);

protected :
	virtual void NativeConstruct() override;

private :
	UFUNCTION()
	void CALLBACK_OnGameOver(float score);
	
};
