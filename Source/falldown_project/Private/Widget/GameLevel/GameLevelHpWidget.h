// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameLevelHpWidget.generated.h"

/**
 * 
 */
UCLASS()
class UGameLevelHpWidget final : public UUserWidget
{
	GENERATED_BODY()
	
protected :
	// 이 위젯의 크기 정보를 담는 SizeBox 위젯입니다.
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class USizeBox* SizeBox_Parent;

	// 현재 Hp 를 나타내기 위한 이미지 위젯입니다.
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage* Image_Hp;

private :
	// 위치 이동을 위해 사용될 슬롯 객체를 나타냅니다.
	UPROPERTY()
	class UCanvasPanelSlot* HpImageSlot;

	// Hp 이미지의 목표 위치를 담게 될 변수
	float HpImageTargetXPosition;

protected :
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public :
	// Hp 위젯을 초기화합니다.
	void InitializeWidget(class AGamePlayerState* playerState);

private :
	// Hp Image 위치를 갱신합니다.
	void UpdateHpImagePosition(float dt);

	// Hp Image 위치를 설정합니다.
	void SetHpImagePosition(float currentHp, float maxHp, bool bForceMove = false);

	// 체력이 변경되었을 경우 호출됩니다.
	// currentHp : 현재 체력이 전달됩니다.
	// maxHp : 최대 체력이 전달됩니다.
	UFUNCTION()
	void CALLBACK_OnHpChanged(float currentHp, float maxHp);

};
