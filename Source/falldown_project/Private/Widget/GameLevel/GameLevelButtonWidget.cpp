#include "Widget/GameLevel/GameLevelButtonWidget.h"
#include "PlayerState/GamePlayerState.h"
#include "Components/Button.h"
#include "Components/Image.h"
#include "Kismet/GameplayStatics.h"

void UGameLevelButtonWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// 일시정지 버튼 클릭 이벤트 바인딩
	Button_Pause->OnClicked.AddUniqueDynamic(
		this, &ThisClass::CALLBACK_OnPauseButtonClicked);

	Button_Home->OnClicked.AddUniqueDynamic(
		this, &ThisClass::CALLBACK_OnHomeButtonClicked);
}

void UGameLevelButtonWidget::InitializeWidget(AGamePlayerState* playerState)
{
	GamePlayerState = playerState;

	// 일시정지 버튼 이미지 갱신
	UpdatePauseButtonImage();
}

void UGameLevelButtonWidget::UpdatePauseButtonImage()
{
	// 일시정지 상태를 얻습니다.
	bool isPaused = GamePlayerState->GetPauseState();

	// 일시정지 이미지 활성화 / 비활성화
	Image_Pause->SetVisibility(isPaused ? 
		ESlateVisibility::Hidden : ESlateVisibility::Visible);

	// 플레이 이미지 활성화 / 비활성화
	Image_Play->SetVisibility(isPaused ?
		ESlateVisibility::Visible : ESlateVisibility::Hidden);

	UpdateHomeButtonVisibility(isPaused);
}

void UGameLevelButtonWidget::UpdateHomeButtonVisibility(bool bVisible)
{
	Button_Home->SetVisibility(bVisible ? 
		ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

void UGameLevelButtonWidget::CALLBACK_OnPauseButtonClicked()
{
	// 일시정지 토글
	GamePlayerState->TogglePause();

	// 일시정지 버튼 이미지 갱신
	UpdatePauseButtonImage();
}

void UGameLevelButtonWidget::CALLBACK_OnHomeButtonClicked()
{
	if (GamePlayerState->GetPauseState())
		GamePlayerState->TogglePause();

	UGameplayStatics::OpenLevel(GetWorld(), TEXT("MainLevel"));
}
