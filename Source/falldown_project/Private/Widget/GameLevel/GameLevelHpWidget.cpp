#include "Widget/GameLevel/GameLevelHpWidget.h"
#include "PlayerState/GamePlayerState.h"
#include "Components/Image.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/SizeBox.h"

void UGameLevelHpWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// Hp 이미지 슬롯을 얻습니다.
	HpImageSlot = Cast<UCanvasPanelSlot>(Image_Hp->Slot);
}

void UGameLevelHpWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
	UpdateHpImagePosition(InDeltaTime);
}

void UGameLevelHpWidget::InitializeWidget(AGamePlayerState* playerState)
{
	playerState->OnHpChanged.AddUObject(
		this, &ThisClass::CALLBACK_OnHpChanged);

	float currentHp = playerState->GetCurrentHp();
	float maxHp = playerState->GetMaxHp();

	// Hp 이미지 위치 설정
	SetHpImagePosition(currentHp, maxHp, true);
}

void UGameLevelHpWidget::UpdateHpImagePosition(float dt)
{
	// 현재 이미지 위치 얻기
	float currentXPosition = HpImageSlot->GetPosition().X;

	// 설정시킬 새로운 X 위치
	float newXPosition = FMath::FInterpConstantTo(
		currentXPosition, HpImageTargetXPosition, dt, 1000.0f);

	// HP 이미지 위치를 설정합니다.
	HpImageSlot->SetPosition(FVector2D(newXPosition, 0.0f));
}

void UGameLevelHpWidget::SetHpImagePosition(float currentHp, float maxHp, bool bForceMove)
{
	// SizeBox 에 정의된 최대 원하는 너비를 얻습니다.
	float maxDesiredWidth = SizeBox_Parent->GetMaxDesiredWidth();

	// HP 이미지가 배치될 위치를 계산합니다.
	float positionX = (currentHp / maxHp) * maxDesiredWidth;

	HpImageTargetXPosition = positionX;

	if (bForceMove)
	{
		// HP 이미지 위치를 설정합니다.
		HpImageSlot->SetPosition(FVector2D(positionX, 0.0f));
	}
}

void UGameLevelHpWidget::CALLBACK_OnHpChanged(float currentHp, float maxHp)
{
	SetHpImagePosition(currentHp, maxHp);
}
