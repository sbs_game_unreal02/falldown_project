#include "Widget/GameLevel/GameLevelWidget.h"
#include "Widget/GameLevel/GameLevelScoreWidget.h"
#include "Widget/GameLevel/GameLevelButtonWidget.h"
#include "Widget/GameLevel/GameLevelHpWidget.h"
#include "Widget/GameLevel/GameLevelGameOverWidget.h"

#include "PlayerState/GamePlayerState.h"

void UGameLevelWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// ���� ���� ������ ����ϴ�.
	//Widget_GameLevelGameOver->SetVisibility(ESlateVisibility::Hidden);

	// ���� �̸��� �̿��Ͽ� ���� ��ü�� ã�� ���
	//GameLevelScoreWidget = Cast<UGameLevelScoreWidget>(
	//	GetWidgetFromName(TEXT("Widget_GameLevelScroe")));
}

void UGameLevelWidget::InitializeWidget(AGamePlayerState* playerState)
{
	Widget_GameLevelScore->InitializeWidget(playerState);
	Widget_GameLevelButton->InitializeWidget(playerState);
	Widget_GameLevelHp->InitializeWidget(playerState);
	Widget_GameLevelGameOver->InitializeWidget(playerState);
}
