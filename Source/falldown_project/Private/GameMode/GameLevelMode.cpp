#include "GameMode/GameLevelMode.h"

#include "PlayerController/GamePlayerController.h"
#include "Actor/PlayerCharacter.h"
#include "PlayerState/GamePlayerState.h"

AGameLevelMode::AGameLevelMode()
{
	//UClass* GameLevelModeClass = AGameLevelMode::StaticClass();
	//TSubclassOf<AGameModeBase> MyGameLevelModeClass = AGameLevelMode::StaticClass();
	//TSubclassOf<AGameModeBase> MyGameLevelModeClass = AActor::StaticClass(); // X

	// 플레이어 컨트롤러 클래스 지정
	PlayerControllerClass = AGamePlayerController::StaticClass();

	// 처음에 사용하게 될 폰 클래스 지정
	DefaultPawnClass = APlayerCharacter::StaticClass();

	// 이 게임모드가 사용되는 레벨에서
	// 사용될 플레이어 스테이트 클래스를 등록합니다.
	PlayerStateClass = AGamePlayerState::StaticClass();
}
