// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameLevelMode.generated.h"

/**
 * 
 */
UCLASS()
class AGameLevelMode final : public AGameModeBase
{
	GENERATED_BODY()
	
public :
	AGameLevelMode();

};
