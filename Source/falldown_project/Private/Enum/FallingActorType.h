// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FallingActorType.generated.h"


UENUM(BlueprintType)
enum class EFallingActorType : uint8
{
	FT_Fish			UMETA(DisplayName = "Fish Type"),
	FT_Trash		UMETA(DisplayName = "Trash Type")
};

// 언리얼에서 사용할 수 있는 열거형식은 C Style / C++ Style 로 나뉩니다.
// enum : C Style 열거형식을 나타내며, Unreal Engine Blueprint 와 호환되지 않습니다.
// 열거 요소명을 형식명과 같이 사용하지 않아도 되기 때문에 C++ 에서만 사용할 수 있는 
// 정수 형태의 상수를 깔끔하게 나타내기 위하여 사용됩니다.
//
// enum class : C++ Style 의 엄격한 열거 형식(strongly typed enum)입니다.
// 기본적으로는 언리얼 블루프린트와 호환되자 않지만, 몇 가지 규칙을 지킨다면 
// C++, Blueprint 모두 사용할 수 있습니다.
// 기본적인 규칙은 다음과 같습니다.
// - UHT 를 위하여 generated.h 파일을 포함시킵니다.
// - UENUM() 매크로와 함께 BlueprintType 지정자를 작성합니다.
// - 1바이트 부호 없는 정수 형식이 기반 형식이 되도록 uint8 형식을 underlying type 으로 지정합니다.
// - UMETA() 매크로와 함께 DisplayName 지정자로 블루프린트에서 표시될 요소명을 작성할 수 있습니다.