#include "Actor/FallingFishActor.h"

#include "Interface/FallingActorCollisionable.h"

void AFallingFishActor::OnCollisionableActorDetected(
	TScriptInterface<IFallingActorCollisionable> collisionableActor)
{
	Super::OnCollisionableActorDetected(collisionableActor);

	collisionableActor->OnFishActorDetected(RecoveryHp);
}
