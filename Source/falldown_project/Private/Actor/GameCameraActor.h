#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameCameraActor.generated.h"

UCLASS()
class AGameCameraActor final : public AActor
{
	GENERATED_BODY()
	class USceneComponent* DefaultSceneRoot;
	
public:	
	AGameCameraActor();

};
