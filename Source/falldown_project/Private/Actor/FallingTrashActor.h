// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/FallingActorBase.h"
#include "FallingTrashActor.generated.h"

/**
 * 쓰레기 액터를 나타냅니다. 
 */
UCLASS()
class AFallingTrashActor final : public AFallingActorBase
{
	GENERATED_BODY()


protected:
	// 이 액터와 충돌 가능한 객체가 감지된 경우 호출됩니다.
	virtual void OnCollisionableActorDetected(
		TScriptInterface<class IFallingActorCollisionable> collisionableActor) override;

	
};
