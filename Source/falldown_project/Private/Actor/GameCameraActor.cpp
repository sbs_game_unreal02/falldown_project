#include "Actor/GameCameraActor.h"

#include "Components/SceneComponent.h"



AGameCameraActor::AGameCameraActor()
{
	DefaultSceneRoot = 
		CreateDefaultSubobject<USceneComponent>(TEXT("DEF_ROOT_COMP"));

	SetRootComponent(DefaultSceneRoot);
}
