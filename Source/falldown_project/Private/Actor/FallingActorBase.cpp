#include "Actor/FallingActorBase.h"

#include "Components/StaticMeshComponent.h"
#include "Interface/FallingActorCollisionable.h"

AFallingActorBase::AFallingActorBase()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_SAMPLE(
		TEXT("/Script/Engine.StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));

	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_COMP"));
	SetRootComponent(MeshComponent);

	if (SM_SAMPLE.Succeeded())
	{
		MeshComponent->SetStaticMesh(SM_SAMPLE.Object);
	}

	// 피직스를 활성화 시킵니다.
	MeshComponent->SetSimulatePhysics(true);
}

void AFallingActorBase::BeginPlay()
{
	Super::BeginPlay();
	
	// 디테일 창의 시뮬레이션 중 히트 이벤트 생성 옵션 활성화
	// 물리적 충돌 이벤트를 생성하도록 합니다.
	MeshComponent->SetNotifyRigidBodyCollision(true);

	MeshComponent->OnComponentHit.AddUniqueDynamic(
		this, &ThisClass::CALLBACK_OnCollisionableActorDetected);
	// DynamicDelegate(동적 대리자) 의 경우 C++, Blueprint 겸용이므로
	// 일반 대리자와 다르게 AddDynamic() 또는 AddUniqueDynamic() 매크로 함수를 통해 등록시켜야 합니다.
	// AddDynamic : 같은 함수를 등록시키는 경우 중복 등록이 가능합니다.
	// AddUniqueDynamic :같은 함수를 등록시켜도 한 개의 함수만 등록됩니다.

	MeshComponent->SetCollisionProfileName(TEXT("FallingActor"));

}

void AFallingActorBase::InitializeFallingActor(
	TScriptInterface<class IFallingActorCollisionable> collisionableActor,
	float hitDamage,
	float recoveryHp,
	float score)
{
	CollisionableActor = collisionableActor;
	Damage = hitDamage;
	RecoveryHp = recoveryHp;
	Score = score;
}

void AFallingActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFallingActorBase::OnCollisionableActorDetected(TScriptInterface<class IFallingActorCollisionable> collisionableActor)
{
	// 점수 변경
	collisionableActor->AddScore(Score);
}

void AFallingActorBase::CALLBACK_OnCollisionableActorDetected(
	UPrimitiveComponent* HitComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	FVector NormalImpulse, 
	const FHitResult& Hit)
{
	// 인터페이스 구현 확인 방식
	//if (OtherActor->GetClass()->ImplementsInterface(UFallingActorCollisionable::StaticClass()))

	// 클래스 상속 확인 방식
	//if (OtherActor->IsA<APlayerCharacter>())

	// 플레이어 캐릭터와 충돌한 경우
	if (OtherActor->ActorHasTag(TEXT("PlayerCharacter")))
	{
		// 충돌 가능 액터 감지
		OnCollisionableActorDetected(CollisionableActor);

		// 이 액터를 제거하도록 합니다.
		Destroy();
	}
	else if (OtherActor->ActorHasTag(TEXT("Floor")))
	{
		// 이 액터를 제거하도록 합니다.
		Destroy();
	}

}

