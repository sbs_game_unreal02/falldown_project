// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "../../falldown_project.h"

#include "Interface/FallingActorCollisionable.h"

#include "PlayerCharacter.generated.h"

UCLASS()
class APlayerCharacter final : public APawn,
	public IFallingActorCollisionable
{
	GENERATED_BODY()

public :
	// 수평 축 입력 시 발생하는 이벤트
	FHorizontalInputEventSignature OnHorizontalInputEvent;


protected :
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCapsuleComponent* CapsuleComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UArrowComponent* ArrowComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USkeletalMeshComponent* BodyComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UPlayerCharacterMovementComponent* MovementComponent;

private :
	// 플레이어 상태 객체
	UPROPERTY()
	class AGamePlayerState* GamePlayerState;





public:
	APlayerCharacter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public :
	// 수평 축 입력이 존재하는 경우 호출됩니다.
	// axis : 축 입력값이 전달됩니다.
	void OnHorizontalInput(float axis);

	// 이동 컴포넌트에 대한 접근자입니다.
	FORCEINLINE class UPlayerCharacterMovementComponent* GetPlayerCharacterMovementComponent() const 
	{ return MovementComponent; }


public :
	// 쓰레기 액터 감지
	virtual void OnTrashActorDetected(float damage) override;

	// 물고기 액터 감지
	virtual void OnFishActorDetected(float recoveryHp) override;

	// 점수 추가
	virtual void AddScore(float score) override;
};
