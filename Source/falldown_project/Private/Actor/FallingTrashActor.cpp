#include "Actor/FallingTrashActor.h"

#include "Interface/FallingActorCollisionable.h"

void AFallingTrashActor::OnCollisionableActorDetected(
	TScriptInterface<IFallingActorCollisionable> collisionableActor)
{
	Super::OnCollisionableActorDetected(collisionableActor);

	collisionableActor->OnTrashActorDetected(Damage);
}
