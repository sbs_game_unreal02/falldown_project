#include "Actor/PlayerCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Component/PlayerCharacterMovementComponent.h"

#include "PlayerState/GamePlayerState.h"

#include "AnimInstance/PlayerCharacterAnimInstance.h"

APlayerCharacter::APlayerCharacter()
{
	// /Script/Engine.StaticMesh'/Engine/BasicShapes/Cylinder.Cylinder'
	// 에 해당하는 에셋을 BodyComponent의 Mesh 로 지정해주세요.
	//static ConstructorHelpers::FObjectFinder<UStaticMesh> SM_CYLINDER(
	//	TEXT("/Script/Engine.StaticMesh'/Engine/BasicShapes/Cylinder.Cylinder'"));

	// 펭귄 SkeletalMesh Asset 불러오기
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SK_PENGUIN(
		TEXT("/Script/Engine.SkeletalMesh'/Game/Assets/Character/SK_Penguin.SK_Penguin'"));

	// 펭귄 애님 인스턴스 블루프린트 클래스 불러오기
	static ConstructorHelpers::FClassFinder<UPlayerCharacterAnimInstance> ANIMBP_PLAYERCHARACTER(
		TEXT("/Script/Engine.AnimBlueprint'/Game/Blueprints/Player/AnimBP_PlayerCharacter.AnimBP_PlayerCharacter_C'"));

	PrimaryActorTick.bCanEverTick = true;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CAPSULE_COMP"));
	ArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ARROW_COMP"));
	BodyComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BODY_COMP"));
	MovementComponent = CreateDefaultSubobject<UPlayerCharacterMovementComponent>(TEXT("MOVEMENT_COMP"));

	SetRootComponent(CapsuleComponent);
	ArrowComponent->SetupAttachment(GetRootComponent());
	BodyComponent->SetupAttachment(GetRootComponent());

	// Capsule Component 절반 높이, 반지름 설정
	CapsuleComponent->SetCapsuleHalfHeight(90.0f);
	CapsuleComponent->SetCapsuleRadius(50.0f);

	// 캡슐 영역과 다른 액터 사이에 동적 물리 충돌이 가능하도록 합니다.
	CapsuleComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));

	// Mesh 초기화
	// Mesh 가 카메라 방향을 바라볼 수 있도록 합니다.
	BodyComponent->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	// Mesh 가 바닥에 닿아있도록 합니다.
	BodyComponent->SetRelativeLocation(FVector::DownVector * 90.0f);

	// Mesh 에서 표시할 SkeletalMesh 에셋 설정
	if (SK_PENGUIN.Succeeded())
	{
		BodyComponent->SetSkeletalMesh(SK_PENGUIN.Object);

		// LOD 강제
		BodyComponent->ForcedLodModel = 1;

		// Mesh 에서 사용될 애님 인스턴스 클래스 지정
		if (ANIMBP_PLAYERCHARACTER.Succeeded())
		{
			BodyComponent->SetAnimInstanceClass(ANIMBP_PLAYERCHARACTER.Class);
		}
	}
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	// 플레이어 상태 객체를 얻습니다.
	GamePlayerState = Cast<AGamePlayerState>(GetPlayerState());

	// 이 액터에 PlayerCharacter 태그를 추가합니다.
	Tags.Add(TEXT("PlayerCharacter"));
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APlayerCharacter::OnHorizontalInput(float axis)
{
	// 대리자의 경우
	//OnHorizontalInputEvent.ExecuteIfBound();

	OnHorizontalInputEvent.Broadcast(axis);
}

void APlayerCharacter::OnTrashActorDetected(float damage)
{
	// 현재 Hp 수치를 얻습니다.
	float currentHp = GamePlayerState->GetCurrentHp();

	// 설정시킬 Hp 수치를 계산합니다.
	float newHp = currentHp - damage;

	// Hp 수치를 설정합니다.
	GamePlayerState->SetCurrentHp(newHp);
}

void APlayerCharacter::OnFishActorDetected(float recoveryHp)
{
	// 현재 Hp 수치를 얻습니다.
	float currentHp = GamePlayerState->GetCurrentHp();

	// 설정시킬 Hp 수치를 계산합니다.
	float newHp = currentHp + recoveryHp;

	// Hp 수치를 설정합니다.
	GamePlayerState->SetCurrentHp(newHp);
}

void APlayerCharacter::AddScore(float score)
{
	// 현재 점수를 얻습니다.
	float currentScore = GamePlayerState->GetGameScore();

	// 점수 변경
	currentScore += score;

	// 변경된 점수를 설정합니다.
	GamePlayerState->SetGameScore(currentScore);

}


