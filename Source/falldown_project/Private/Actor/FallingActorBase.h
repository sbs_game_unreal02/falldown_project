// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FallingActorBase.generated.h"

UCLASS(Abstract)
class AFallingActorBase : public AActor
{
	GENERATED_BODY()
	
private :
	// 이 액터와 충돌 가능한 객체의 기능을 나타냅니다.
	UPROPERTY()
	TScriptInterface<class IFallingActorCollisionable> CollisionableActor;

	// 캐릭터와 충돌 시 변화시킬 점수량입니다.
	UPROPERTY()
	float Score;

protected :
	// 캐릭터와 충돌 시 캐릭터가 회복하는 수치를 나타냅니다.
	UPROPERTY()
	float RecoveryHp;

	// 캐릭터와 충돌 시 캐릭터에게 가해질 피해량을 나타냅니다.
	UPROPERTY()
	float Damage;


protected :
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent * MeshComponent;

public:	
	AFallingActorBase();

protected:
	virtual void BeginPlay() override;

public:	
	// 이 액터의 내용을 초기화합니다.
	void InitializeFallingActor(
		TScriptInterface<class IFallingActorCollisionable> collisionableActor,
		float hitDamage,
		float recoveryHp,
		float score);

	virtual void Tick(float DeltaTime) override;

protected :
	// 이 액터와 충돌 가능한 객체가 감지된 경우 호출됩니다.
	virtual void OnCollisionableActorDetected(
		TScriptInterface<class IFallingActorCollisionable> collisionableActor);


private :
	UFUNCTION()
	void CALLBACK_OnCollisionableActorDetected(
		UPrimitiveComponent* HitComponent, // 이 액터에서 충돌이 발생한 컴포넌트가 전달됩니다.
		AActor* OtherActor, 			   // 이 액터와 충돌이 발생한 상태 액터가 전달됩니다.
		UPrimitiveComponent* OtherComp,    // 이 액터와 충돌이 발생한 상대 컴포넌트가 전달됩니다.
		FVector NormalImpulse, 			   // 충격에 의해 튕겨지는 속도가 전달됩니다.
		const FHitResult& Hit);			   // 충돌에 대한 자세한 정보가 전달됩니다.

};
