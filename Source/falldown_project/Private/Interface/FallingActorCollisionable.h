// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FallingActorCollisionable.generated.h"

// MinimalAPI : 클래스의 형태 정보만 다른 모듈에서 사용할 수 있도록 Export
// 클래스 형식 변환이 가능하지만, 그 클래스의 인라인 함수를 제외한 함수는 호출할 수 없습니다.
// BlueprintType : 이 형식을 블루프린트에서 변수 형태로 사용할 수 있습니다.
UINTERFACE(MinimalAPI)
class UFallingActorCollisionable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 이 곳에 내용을 정의합니다.
 */
class IFallingActorCollisionable
{
	GENERATED_BODY()

public:
	// 쓰레기 액터가 감지되는 경우 호출됩니다.
	virtual void OnTrashActorDetected(float damage) = 0;

	// 물고기 액터가 감지되는 경우 호출됩니다.
	virtual void OnFishActorDetected(float recoveryHp) = 0;

	// 점수를 추가합니다.
	virtual void AddScore(float score) PURE_VIRTUAL(ThisClass::AddScore, );

	//virtual bool IsActive() PURE_VIRTUAL(IFallingActorCollisionable::IsActive, return false;);
};
