#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Enum/FallingActorType.h"
#include "FallingActorInfoTableRow.generated.h"

/**
 * 게임 제작 시 필요한 경험치량, 스킬의 계수 등 이러한 밸런스와 관련된 내용들을
 * 스크립트에 함부로 작성하게 되면 추후 밸런스 작업 시 굉장히 불편해지며, 
 * 협업에 문제가 생길 수도 있습니다.
 * 이러한 기획과 관련된 내용은 프로그래머가 아닌 다른 작업자가 수정하기 편하도록 
 * 에셋 형태로 설계하는 것이 좋습니다.
 * 
 * 언리얼 엔진에서는 프로그래머와 기획자가 작업한 내용을 게임으로 쉽게 옮김 수 있도록 
 * DataTable 방식을 제공합니다.
 * DataTable 에서는 CSV 파일, JSON 파일에 대한 Import/Export 기능을 제공하며 
 * 기본적인 형태만 설계된다면 누구든지 게임에 사용되는 수치 데이터들을 쉽게 
 * 추가/제거하거나 조정할 수 있습니다.
 * 
 * 언리얼 엔진에서 DataTable 형태를 설계하는 방법은 다음과 같습니다.
 * Blueprint나 C++ 에서 구조체를 작성하여 형태를 설계하여 DataTable 의 형태를 설계합니다.
 * 
 * C++ 에서 DataTable 형태를 위해 구조체를 작성하는 경우 몇가지 규칙을 따라야 합니다.
 * 1. UHT 를 위한 generated.h 포함.
 * 2. USTRUCT() 매크로와 BlueprintType 지정자 작성.
 * 3. FTableRowBase 구조체를 기반 형식으로 지정.
 */
USTRUCT(BlueprintType)
struct FFallingActorInfoTableRow : public FTableRowBase
{
	GENERATED_BODY()


public :
	// 생성시킬 액터의 블루프린트 클래스
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Blueprint Class")
	TSubclassOf<class AFallingActorBase> FallingActorBlueprintClass;
	
	// 액터 타입을 나타냅니다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	EFallingActorType FallingActorType;

	// 피해량을 나타냅니다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float HitDamage;

	// 체력 회복량을 나타냅니다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float RecoveryHp;

	// 점수 변화량을 나타냅니다.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
	float Score;
};
