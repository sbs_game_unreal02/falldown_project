// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "GeneratorSectionInfoTableRow.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FGeneratorSectionInfoTableRow : public FTableRowBase
{
	GENERATED_BODY()

public:
	// 섹션 시작 점수
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Section Info")
	int32 SectionStartScore;

	// 딜레이
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Genrate Option")
	float GeneratingDelay;

	// 물고기 생성 확률
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Genrate Option")
	int32 FishGenerationPercentage;
};
