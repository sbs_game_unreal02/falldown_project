#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BasePlayerController.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class ABasePlayerController : public APlayerController
{
	GENERATED_BODY()

protected :
	void InitializeCameraView();

public :
	// 점수를 로드합니다.
	float LoadHighScore();

	// 점수를 저장합니다.
	void SaveHighScore(float score);
	
};
