#include "PlayerController/GamePlayerController.h"

#include "Actor/PlayerCharacter.h"
#include "Actor/GameCameraActor.h"
#include "Widget/GameLevel/GameLevelWidget.h"
#include "PlayerState/GamePlayerState.h"
#include "SaveGame/HighScoreSaveGame.h"

#include "Kismet/GameplayStatics.h"

#include "TimerManager.h"

AGamePlayerController::AGamePlayerController()
{
	// 위젯 블루프린트 클래스를 불러옵니다.
	static ConstructorHelpers::FClassFinder<UGameLevelWidget> WIDGETBP_GAMELEVEL(
		TEXT("/Script/UMGEditor.WidgetBlueprint'/Game/Blueprints/Widget/WidgetBP_GameLevel.WidgetBP_GameLevel_C'"));

	// 게임 레벨 위젯 클래스를 저장합니다.
	if (WIDGETBP_GAMELEVEL.Succeeded())
	{
		GameLevelWidgetClass = WIDGETBP_GAMELEVEL.Class;
	}
}

void AGamePlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis(
		TEXT("HorizontalInput"), this, &ThisClass::CALLBACK_OnHorizontalInput);


}

void AGamePlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	// 플레이어 캐릭터 객체를 얻습니다.
	PlayerCharacter = Cast<APlayerCharacter>(aPawn);

	// 카메라 뷰를 초기화합니다.
	InitializeCameraView();
}

void AGamePlayerController::BeginPlay()
{
	Super::BeginPlay();

	AGamePlayerState* playerState = GetPlayerState<AGamePlayerState>();

	// 일시정지 콜백 등록
	playerState->OnPaused.AddUObject(this, &ThisClass::CALLBACK_OnPaused);

	// 게임 오버 콜백 등록
	playerState->OnGameOver.AddUObject(this, &ThisClass::CALLBACK_OnGameOver);

	// 게임 위젯 초기화
	InitializeGameWidget();

	// 커서를 표시합니다.
	bShowMouseCursor = true;

	// 입력 모드를 설정합니다.
	SetInputMode(FInputModeGameAndUI());
}

void AGamePlayerController::InitializeGameWidget()
{
	// 플레이어 스테이트 객체를 얻습니다.
	AGamePlayerState* playerState = GetPlayerState<AGamePlayerState>();

	// 위젯 객체를 생성합니다.
	UGameLevelWidget* gameLevelWidget = CreateWidget<UGameLevelWidget>(this, GameLevelWidgetClass);

	// 화면에 위젯을 띄웁니다.
	gameLevelWidget->AddToViewport();

	// 위젯 객체 초기화
	gameLevelWidget->InitializeWidget(playerState);
}

void AGamePlayerController::CALLBACK_OnHorizontalInput(float axis)
{
	//APlayerCharacter* playerCharacter = Cast<APlayerCharacter>(GetPawn());
	// GetPawn() : 플레이어 컨트롤러가 조종중인 폰 객체를 APawn* 형식으로 반환합니다.


	if (IsValid(PlayerCharacter))
	{
		// IsValid(object) : object 가 유효한 객체인지 확인하여 
		// bool 형식으로 결과를 반환합니다.

		PlayerCharacter->OnHorizontalInput(axis);
	}
}

void AGamePlayerController::CALLBACK_OnPaused(bool isPaused)
{
	SetPause(isPaused);
}

void AGamePlayerController::CALLBACK_OnGameOver(float score)
{
	FTimerHandle gameOverTimerHandle;

	GetWorld()->GetTimerManager().SetTimer(
		gameOverTimerHandle, 
		FTimerDelegate::CreateLambda([&]() {
			UGameplayStatics::OpenLevel(GetWorld(), TEXT("MainLevel"));
			}),
		3.0f, false);

	// 이전에 세운 최고 점수를 얻습니다.
	float prevHighScore = LoadHighScore();

	// 더 높은 점수를 기록했다면, 점수를 저장합니다.
	if (prevHighScore < score)
		SaveHighScore(score);
}
