#pragma once

#include "CoreMinimal.h"
#include "PlayerController/BasePlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AMainPlayerController final : public ABasePlayerController
{
	GENERATED_BODY()

private :
	// WidgetBP_MainLevel 클래스
	UPROPERTY()
	TSubclassOf<class UMainLevelWidget> MainLevelWidgetClass;

public :
	AMainPlayerController();

protected :
	virtual void BeginPlay() override;

private :
	// MainLevel 위젯을 초기화합니다.
	void InitializeMainWidget();
};
