#include "MainPlayerController.h"
#include "Widget/MainLevel/MainLevelWidget.h"

AMainPlayerController::AMainPlayerController()
{
	static ConstructorHelpers::FClassFinder<UMainLevelWidget> WIDGETBP_MAINLEVEL(
		TEXT("/Script/UMGEditor.WidgetBlueprint'/Game/Blueprints/Widget/WidgetBP_MainLevel.WidgetBP_MainLevel_C'"));

	if (WIDGETBP_MAINLEVEL.Succeeded())
	{
		MainLevelWidgetClass = WIDGETBP_MAINLEVEL.Class;
	}
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	// 카메라 뷰 초기화
	InitializeCameraView();

	// MainLevel 위젯 초기화
	InitializeMainWidget();

	// 커서 표시
	bShowMouseCursor = true;

	// UI 입력 모드로 설정합니다.
	SetInputMode(FInputModeUIOnly());
}

void AMainPlayerController::InitializeMainWidget()
{
	// 위젯 생성
	UMainLevelWidget* mainLevelWidget = CreateWidget<UMainLevelWidget>(
		this, MainLevelWidgetClass);

	// 위젯을 화면에 표시합니다.
	mainLevelWidget->AddToViewport();

	// 기록한 최고 점수를 얻습니다.
	float highScore = LoadHighScore();

	mainLevelWidget->InitializeWidget(highScore);
}
