#include "PlayerController/BasePlayerController.h"

#include "Actor/GameCameraActor.h"
#include "Kismet/GameplayStatics.h"
#include "SaveGame/HighScoreSaveGame.h"

void ABasePlayerController::InitializeCameraView()
{
	// 찾은 액터를 담아둘 배열
	TArray<AActor*> findedActors;

	// 월드에서 AGameCameraActor 형식의 액터를 모두 찾습니다.
	UGameplayStatics::GetAllActorsOfClass(
		this, AGameCameraActor::StaticClass(), findedActors);
	// UGameplayStatics : 블루프린트/UnrealC++ 에서 사용할 수 있는 유용한 기능들을
	// 제공해주는 유틸리티 정적 클래스

	// 하나 이상의 액터를 찾은 경우
	if (findedActors.Num() > 0)
	{
		// 뷰 타깃 액터를 얻습니다.
		AActor* viewTargetActor = findedActors[0];

		// 뷰 타깃을 설정합니다.
		SetViewTarget(viewTargetActor);
	}
}

float ABasePlayerController::LoadHighScore()
{
	// 저장데이터 존재 유무를 확인합니다.
	UHighScoreSaveGame* savedGameData = Cast<UHighScoreSaveGame>(
		UGameplayStatics::LoadGameFromSlot(
			UHighScoreSaveGame::SlotName,
			UHighScoreSaveGame::UserIndex));

	// 저장 데이터가 존재하는 경우
	if (IsValid(savedGameData))
	{
		return savedGameData->HighScore;
	}
	// 저장 데이터를 찾을 수 없는 경우
	else
	{
		SaveHighScore(INDEX_NONE);
		return INDEX_NONE;
	}
}

void ABasePlayerController::SaveHighScore(float score)
{
	// 저장 데이터 객체 생성
	UHighScoreSaveGame* savedGameData = Cast<UHighScoreSaveGame>(
		UGameplayStatics::CreateSaveGameObject(UHighScoreSaveGame::StaticClass()));

	// 점수 저장
	savedGameData->HighScore = score;
	UGameplayStatics::SaveGameToSlot(
		savedGameData,
		UHighScoreSaveGame::SlotName,
		UHighScoreSaveGame::UserIndex);
}
