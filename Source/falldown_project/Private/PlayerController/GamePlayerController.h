#pragma once

#include "CoreMinimal.h"
#include "PlayerController/BasePlayerController.h"
#include "GamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AGamePlayerController final : public ABasePlayerController
{
	GENERATED_BODY()

private :
	// WidgetBP_GameLevel 블루프린트 클래스입니다.
	UPROPERTY()
	TSubclassOf<class UGameLevelWidget> GameLevelWidgetClass;

	// 이 컨트롤러가 조종하는 플레이어 캐릭터 객체를 나타냅니다.
	UPROPERTY()
	class APlayerCharacter* PlayerCharacter;

public :
	AGamePlayerController();

protected :
	virtual void SetupInputComponent() override;

	// 이 PlayerController 객체가 Pawn 객체에 빙의되었을 때 호출되는 멤버 함수
	// aPawn 매개 변수로 빙의된 폰 객체가 전달됩니다.
	virtual void OnPossess(APawn* aPawn) override;

	virtual void BeginPlay() override;

private :
	// 게임 위젯 초기화
	void InitializeGameWidget();

	// HorizontalInput 축 입력에 바인딩 될 멤버 함수입니다.
	UFUNCTION()
	void CALLBACK_OnHorizontalInput(float axis);

	// 일시정지 시 호출됩니다.
	UFUNCTION()
	void CALLBACK_OnPaused(bool isPaused);

	// 게임 오버 시 호출됩니다.
	UFUNCTION()
	void CALLBACK_OnGameOver(float score);
};
