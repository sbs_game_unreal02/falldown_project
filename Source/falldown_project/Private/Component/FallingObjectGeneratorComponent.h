// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FallingObjectGeneratorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UFallingObjectGeneratorComponent final : public UActorComponent
{
	GENERATED_BODY()

private:
	// 떨어지는 오브젝트에 대한 정보를 담는 DataTable Asset
	UPROPERTY()
	class UDataTable* FallingActorInfoDataTable;

	// 생성기 구간 정보를 담는 DataTable Asset
	UPROPERTY()
	class UDataTable* GeneratorSectionInfoDataTable;

	UPROPERTY()
	class AGamePlayerState* PlayerState;

	UPROPERTY()
	TScriptInterface<class IFallingActorCollisionable> CollisionableActor;
	// class IFallingActorCollisionable *

	// 생성 범위 왼쪽 위치 Y
	UPROPERTY()
	float GeneratingLocationLeftY;

	// 생성 범위 오른쪽 위치 Y
	UPROPERTY()
	float GeneratingLocationRightY;

	// 생성기 타이머 핸들
	UPROPERTY()
	FTimerHandle ActorGeneratingTimerHandle;

	// 생성 가능한 물고기 액터 정보를 담을 배열
	TArray<const struct FFallingActorInfoTableRow*> FallingFishActorInfos;

	// 생성 가능한 쓰레기 액터 정보를 담을 배열
	TArray<const struct FFallingActorInfoTableRow*> FallingTrashActorInfos;

	// 이전 구간 인덱스
	int32 PrevSectionIndex;

	// 현재 적용된 구간 인덱스
	int32 SectionIndex;

	// 물고기 생성 확률
	int32 FishGenerationPercentage;

	// 구간 시작 점수 배열을 나타냅니다.
	TArray<int32> SectionStartScores;

	// 구간 정보를 나타냅니다.
	TMap<int32, const struct FGeneratorSectionInfoTableRow*> SectionInfos;





public:	
	UFallingObjectGeneratorComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(
		float dt,
		ELevelTick tickType,
		FActorComponentTickFunction* fnTick) override;

private :
	// 인덱스에 해당하는 구간 정보를 반환합니다.
	const struct FGeneratorSectionInfoTableRow* GetSectionInfoFromIndex(int32 sectionIndex);

	// 생성시킬 액터 정보 배열을 초기화합니다.
	void InitializeFallingActorInfoArrays();

	// 생성기 구간 정보를 초기화합니다.
	void InitializeGeneratorSectionInfos();

	// 생성 범위를 초기화합니다.
	void InitializeGeneratingRage();

	// 생성기 상태를 갱신합니다.
	void UpdateGeneratorState();

	// 떨어지는 오브젝트를 생성합니다.
	void GenerateFallingActor();

	// 액터 타입에 따른 랜덤한 요소를 반환합니다.
	const struct FFallingActorInfoTableRow& GetFallingActorInfo(
		EFallingActorType fallingActorType) const;

	// 타이머 틱
	UFUNCTION()
	void CALLBACK_OnGeneratingTimerTicken();

	// 점수가 변경될 때마다 호출되는 함수
	UFUNCTION()
	void CALLBACK_OnScoreChanged(float score);


};
