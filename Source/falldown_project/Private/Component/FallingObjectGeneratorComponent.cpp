#include "Component/FallingObjectGeneratorComponent.h"

#include "Actor/FallingActorBase.h"
#include "Level/GameLevelScriptActor.h"
#include "TableRow/FallingActorInfoTableRow.h"
#include "TableRow/GeneratorSectionInfoTableRow.h"
#include "PlayerState/GamePlayerState.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Interface/FallingActorCollisionable.h"

#include "TimerManager.h"


UFallingObjectGeneratorComponent::UFallingObjectGeneratorComponent()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> DT_FALLINGACTORINFO(
		TEXT("/Script/Engine.DataTable'/Game/DataTables/DT_FallingActorInfo.DT_FallingActorInfo'"));
	if (DT_FALLINGACTORINFO.Succeeded())
		FallingActorInfoDataTable = DT_FALLINGACTORINFO.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable> DT_GENERATORSECTIONINFO(
		TEXT("/Script/Engine.DataTable'/Game/DataTables/DT_GeneratorSectionInfo.DT_GeneratorSectionInfo'"));
	if (DT_GENERATORSECTIONINFO.Succeeded())
		GeneratorSectionInfoDataTable = DT_GENERATORSECTIONINFO.Object;


	PrimaryComponentTick.bCanEverTick = true;

	// 인덱스 설정
	PrevSectionIndex = INDEX_NONE;
	SectionIndex = 0;
}

void UFallingObjectGeneratorComponent::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> findedActors;

	PlayerState = Cast<AGamePlayerState>(
		UGameplayStatics::GetPlayerState(GetWorld(), 0));

	// 점수 변경 콜백 등록
	PlayerState->OnScoreChanged.AddUObject(
		this, &ThisClass::CALLBACK_OnScoreChanged);

	// 충돌 가능 객체 찾기
	UGameplayStatics::GetAllActorsWithInterface(
		GetWorld(),
		UFallingActorCollisionable::StaticClass(),
		findedActors);

	if (findedActors.Num() > 0)
	{
		CollisionableActor = findedActors[0];
	}

	

	// 생성기 구간 정보 초기화
	InitializeGeneratorSectionInfos();

	// 물고기 / 쓰레기 액터 정보 배열을 초기화합니다.
	InitializeFallingActorInfoArrays();

	// 생성 범위 초기화
	InitializeGeneratingRage();

	// 생성기 상태 갱신
	UpdateGeneratorState();
}

void UFallingObjectGeneratorComponent::TickComponent(
	float dt, ELevelTick tickType, FActorComponentTickFunction* fnTick)
{
	Super::TickComponent(dt, tickType, fnTick);

	// 생성기 상태 갱신
	UpdateGeneratorState();
}


const FGeneratorSectionInfoTableRow* UFallingObjectGeneratorComponent::GetSectionInfoFromIndex(int32 sectionIndex)
{
	// 구간 시작 점수를 얻습니다.
	int32 sectionStartScore = SectionStartScores[sectionIndex];

	// 구간 시작 점수와 일치하는 구간 정보를 얻습니다.
	const FGeneratorSectionInfoTableRow* sectionInfo = SectionInfos[sectionStartScore];

	// 구간 정보를 반환합니다.
	return sectionInfo;
}

void UFallingObjectGeneratorComponent::InitializeFallingActorInfoArrays()
{
	// 데이터 테이블에서 모든 데이터를 배열로 얻습니다.
	FString contextString;
	TArray<FFallingActorInfoTableRow*> fallingActorInfos;
	FallingActorInfoDataTable->GetAllRows<FFallingActorInfoTableRow>(
		contextString, fallingActorInfos);

	// 모든 액터 정보를 확인하고, 타입에 따라 배열에 액터 정보를 추가합니다.
	for (const FFallingActorInfoTableRow* fallingActorInfo : fallingActorInfos)
	{
		// FallingActor 타입에 따라 분기시켜 배열에 정보들을 할당시킵니다.
		switch (fallingActorInfo->FallingActorType)
		{
		// 물고기 타입인 경우
		case EFallingActorType::FT_Fish :
			FallingFishActorInfos.Add(fallingActorInfo);
			break;

		// 쓰레기 타입인 경우
		case EFallingActorType::FT_Trash:
			FallingTrashActorInfos.Add(fallingActorInfo);
			break;
		}
	}
}

void UFallingObjectGeneratorComponent::InitializeGeneratorSectionInfos()
{
	TArray<FGeneratorSectionInfoTableRow*> infos;

	FString contextString;
	GeneratorSectionInfoDataTable->GetAllRows<FGeneratorSectionInfoTableRow>(
		contextString, infos);

	for (FGeneratorSectionInfoTableRow* info : infos)
	{
		// 구간 시작 점수
		int32 sectionStartScore = info->SectionStartScore;

		// 구간 점수를 추가합니다.
		SectionStartScores.Add(sectionStartScore);

		// 점수와 일치하는 구간 정보를 추가합니다.
		SectionInfos.Add(sectionStartScore, info);
	}

}

void UFallingObjectGeneratorComponent::InitializeGeneratingRage()
{
	// 레벨 스크립트 액터를 얻습니다.
	AGameLevelScriptActor* levelScriptActor = Cast<AGameLevelScriptActor>(
		GetWorld()->GetLevelScriptActor());

	// 맵 왼족, 오른쪽 위치를 얻습니다.
	FVector leftLocation, rightLocation;
	levelScriptActor->GetLeftRightPoint(leftLocation, rightLocation);

	// 생성 범위를 설정합니다.
	GeneratingLocationLeftY = leftLocation.Y;
	GeneratingLocationRightY = rightLocation.Y;

}

void UFallingObjectGeneratorComponent::UpdateGeneratorState()
{
	if (PrevSectionIndex == SectionIndex) return;

	PrevSectionIndex = SectionIndex;

	GetWorld()->GetTimerManager().ClearTimer(ActorGeneratingTimerHandle);

	// 현재 구간 정보를 얻습니다.
	const FGeneratorSectionInfoTableRow* currentSectionInfo = GetSectionInfoFromIndex(SectionIndex);

	// 물고기 생성 확률 설정
	FishGenerationPercentage = currentSectionInfo->FishGenerationPercentage;

	// 딜레이
	float delay = currentSectionInfo->GeneratingDelay;

	// 타이머 실행
	// 타이머란 지정한 시간마다 함수를 호출하는 기능입니다.
	// 타이머 실행 시 전달하는 타이머 핸들을 이용하여 실행한 타이머를 제어할 수 있습니다.
	GetWorld()->GetTimerManager().SetTimer(
		ActorGeneratingTimerHandle,
		FTimerDelegate::CreateUObject(
			this, &ThisClass::CALLBACK_OnGeneratingTimerTicken), 
			delay,
			true);
}

void UFallingObjectGeneratorComponent::GenerateFallingActor()
{
	// 물고기 생성 확률을 지정합니다.
	int fishGenPercentage = 50;

	// 확률에 따라 생성시킬 액터 타입을 결정합니다.
	EFallingActorType genActorType = (FMath::RandRange(1, 100) < FishGenerationPercentage) ?
		EFallingActorType::FT_Fish : EFallingActorType::FT_Trash;


	// 생성시킬 액터의 정보를 랜덤하게 얻습니다.
	const FFallingActorInfoTableRow& fallingActorInfo = GetFallingActorInfo(genActorType);

	// 생성 위치를 정의합니다.
	FVector generatorLocation = GetOwner()->GetActorLocation();
	generatorLocation.Y = FMath::RandRange(GeneratingLocationLeftY, GeneratingLocationRightY);

	// 액터 생성 트랜스폼 정보
	FTransform fallingActorSpawnTransform = FTransform(generatorLocation);
	fallingActorSpawnTransform.SetScale3D(FVector::One() * 1.5f);

	// 액터 생성
	AFallingActorBase * fallingActor = 
		GetOwner()->GetWorld()->SpawnActorDeferred<AFallingActorBase>(
		//AFallingActorBase::StaticClass(),
		fallingActorInfo.FallingActorBlueprintClass,
		fallingActorSpawnTransform);

	if (IsValid(fallingActor))
	{
		// 액터 초기화
		fallingActor->InitializeFallingActor(
			CollisionableActor,
			fallingActorInfo.HitDamage,
			fallingActorInfo.RecoveryHp,
			fallingActorInfo.Score);

		// 액터 생성 루틴 종료
		fallingActor->FinishSpawning(fallingActorSpawnTransform);
	}

	// SpawnActorDeferred : 생성자 호출 이후 FinishSpawning() 함수를 호출하지 않음.
	// SpawnActor : 생성자 호출 이후 FinishSpawning() 함수를 호출하여 BeginPlay() 함수가 호출됨.

}

const FFallingActorInfoTableRow& UFallingObjectGeneratorComponent::GetFallingActorInfo(
	EFallingActorType fallingActorType) const
{
	// 매개 변수로 전달된 액터 타입에 따라 사용될 배열을 지정합니다.
	const TArray<const FFallingActorInfoTableRow*>& targetArray =
		(fallingActorType == EFallingActorType::FT_Fish) ?
		FallingFishActorInfos : FallingTrashActorInfos;

	// 랜덤한 요소를 반환하기 위한 인덱스를 결정합니다.
	int32 randomIndex = FMath::RandRange(0, targetArray.Num() - 1);

	// 랜덤한 요소를 반환합니다.
	return *targetArray[randomIndex];
}

void UFallingObjectGeneratorComponent::CALLBACK_OnGeneratingTimerTicken()
{
	// 액터 생성
	GenerateFallingActor();
}

void UFallingObjectGeneratorComponent::CALLBACK_OnScoreChanged(float score)
{
	// 다음 구간이 존재하는 경우에만 확인합니다.
	if (SectionStartScores.Num() > SectionIndex + 1)
	{
		// 다음 구간 점수
		int32 nextSectionStartScore = SectionStartScores[SectionIndex + 1];

		if (nextSectionStartScore <= score)
		{
			// 다음 구간으로 진입합니다.
			++SectionIndex;
		}
	}
}

