// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../../falldown_project.h"
#include "PlayerCharacterMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UPlayerCharacterMovementComponent final : public UActorComponent
{
	GENERATED_BODY()

private :
	UPROPERTY()
	class APlayerCharacter * PlayerCharacter;

	// 이동 속력
	float MoveSpeed;

	// 수평 축 입력값을 기록하기 위한 필드
	float HorizontalAxisValue;

	// 최소 이동 가능 위치Y
	float MoveableLocationMinY;

	// 최대 이동 가능 위치Y
	float MoveableLocationMaxY;

	// 초기 Yaw 회전값
	float InitialRotationYaw;

public:	
	UPlayerCharacterMovementComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private :
	// 이동을 이 곳에서 구현합니다.
	// deltaTime : 프레임 사이 시간 간격이 전달됩니다.
	void Movement(float deltaTime);

	// 회전을 이 곳에서 구현합니다.
	void Rotation();

public :
	// 최대 이동 속력에 대한 접근자입니다.
	FORCEINLINE float GetMoveSpeed() const { return MoveSpeed; }

private :
	UFUNCTION()
	void CALLBACK_OnHorizontalInput(float axis);
		
};
