#include "Component/PlayerCharacterMovementComponent.h"

#include "Actor/PlayerCharacter.h"

#include "Level/GameLevelScriptActor.h"

UPlayerCharacterMovementComponent::UPlayerCharacterMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	// 이동 속력을 정의합니다.
	MoveSpeed = 1300.0f;
}

void UPlayerCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	PlayerCharacter = Cast<APlayerCharacter>(GetOwner());

	PlayerCharacter->OnHorizontalInputEvent.AddUObject(
		this, &ThisClass::CALLBACK_OnHorizontalInput);

	// 플레이어 캐릭터 액터의 초기 Yaw 회전값을 기록합니다.
	InitialRotationYaw = PlayerCharacter->GetActorRotation().Yaw;

	AGameLevelScriptActor* levelScriptActor =
		Cast<AGameLevelScriptActor>(PlayerCharacter->GetWorld()->GetLevelScriptActor());

	if (IsValid(levelScriptActor))
	{
		FVector leftLocation, rightLocation;
		levelScriptActor->GetLeftRightPoint(leftLocation, rightLocation);

		MoveableLocationMinY = leftLocation.Y;
		MoveableLocationMaxY = rightLocation.Y;
	}
}

void UPlayerCharacterMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Movement(DeltaTime);
	Rotation();
}

void UPlayerCharacterMovementComponent::Movement(float deltaTime)
{
	// 현재 위치를 얻습니다.
	FVector currentLocation = GetOwner()->GetActorLocation();
	
	// 이동할 방향을 생성합니다.
	FVector direction = FVector::RightVector * HorizontalAxisValue;

	// 설정시킬 새로운 위치를 계산합니다.
	FVector newLocation = currentLocation + (direction * MoveSpeed * deltaTime);

	// minY ~ maxY 사이의 위치로 변환합니다.
	newLocation.Y = FMath::Clamp(
		newLocation.Y, MoveableLocationMinY, MoveableLocationMaxY);

	// 계산한 위치를 설정합니다.
	GetOwner()->SetActorLocation(newLocation);
}

void UPlayerCharacterMovementComponent::Rotation()
{
	// 할당시킬 Yaw 회전값
	float newYawAngle = InitialRotationYaw + (HorizontalAxisValue * -90);

	// PlayerCharacter 액터의 회전을 설정합니다.
	PlayerCharacter->SetActorRotation(FRotator(0.0f, newYawAngle, 0.0f));

}

void UPlayerCharacterMovementComponent::CALLBACK_OnHorizontalInput(float axis)
{
	// 입력 값을 기록합니다.
	HorizontalAxisValue = axis;
}

