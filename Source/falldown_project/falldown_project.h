#pragma once

#include "CoreMinimal.h"

// 대리자 형식 선언
DECLARE_EVENT_OneParam(APlayerCharacter, FHorizontalInputEventSignature, float)
// 대리자 : 멤버 함수를 보다 효율적으로 사용하기 위하여 멤버 함수 자체를
// 캡슐화시킬 수 있도록 하는 방법
// 대리자를 이용하여 특정 클래스 내에 있는 멤버 함수를 저장하여
// 멤버 함수를 호출하기 위하여 해당 멤버를 가지고 있는 개체를 통해서가 아닌
// 대리자를 이용할 수 있습니다.
// DECLARE_DELEGATE() : 싱글 캐스트 방식
// DECLARE_MULTICAST_DELEGATE() : 멀티 캐스트 방식
// DECLARE_DELEGATE_OneParam()
// DECLARE_DELEGATE_TwoParams()
// DECLARE_DELEGATE_ThreeParams()...
// DECLARE_DELEGATE_RetVal() : 반환 형식이 void 형식이 아닌! 멤버 함수를 등록시킬 수 있는 대리자 형식 선언
// DECLARE_DELEGATE_RetVal_OneParam(ReturnType, DelegateSignature, ParamType)
// DECLARE_DYNAMIC_DELEGATE() : 블루프린트 함수를 등록시킬 수 있는 싱글 캐스트 대리자 형식 선언
// ex) DECLARE_DYNAMIC_MULTICAST_DELEGATE_RetVal_TwoParams ... 
// 
// 대리자의 장점
// 대리자를 이용하게 되면, 객체간의 의존성을 낮출 수 있으며 이로 인해 더 깔끔하고, 
// 유연한 프로그래밍이 가능해집니다.
// 
// 대리자의 단점
// 외부에서 대리자에 접근하는 경우, 어떤 객체든 해당 대리자를 호출시킬 수 있기 때문에
// 정확히 어느 위치에서 대리자에 등록시킨 멤버 함수가 호출되는지 알기 힘들어집니다.
// 이 때문에 특정 객체에서만 사용되는 대리자의 경우 이벤트를 사용하여 
// 지정된 클래스 내부에서만 호출할 수 있도록 할 수 있습니다.

// 이벤트
// 객체 내에 특정한 상황이 발생했을 경우 오부의 객체에 알리기 위해 사용됩니다.
// DECLARE_EVENT(OwningType, EventName) 매크로 함수를 통해 이벤트 형식을 선언할 수 있습니다.
// 이렇게 선언된 형식으로 생성된 이벤트는 OwningType 클래스 내부에서만 호출할 수 있습니다.
// 기본적으로 이벤트는 멀티 캐스트 방식입니다.



// Multicast void() delegate signature
DECLARE_MULTICAST_DELEGATE(FActionSignature)

